<?php

/**
 * @Property: gregoriobolivar.com.ve
 * @Author: gregoriobolivar.com.ve
 * @email: webmaster@gregoriobolivar.com.ve
 * @Creation Date: 01/12/2015
 * @Audited by: Gregorio J Bolívar B
 * @Description: Código encargado de comprimir el cotenido de tu website.
 * @package: CommunController.php
 * @version: 0.1
 */

class CommunController {

	public function __construct(){
		//  Activa el almacenamiento en búfer de la salida
		ob_start();
		ob_end_flush();
		
	}
	// Comprimir codigos html resutl a vista
	function compressResponce($html)
	{
		$sear = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
		$repl = array('>','<','\\1');
		return preg_replace($sear, $repl, $html);
	}
}
$html = file_get_contents('http://www.sumejorsolucion.com.ve');
//echo $html; // HTML normal
$compr = new CommunController();
echo $compr->compressResponce($html); // HTML COMPRIMIDO

?>
